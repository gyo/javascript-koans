module.exports = {
  plugins: ["jest"],
  extends: ["eslint:recommended"],
  env: {
    browser: true,
    node: true,
    "jest/globals": true
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module"
  },
  rules: {
    "no-console": 0
  }
};
