# JavaScript Koans

Kotlin Koans のパクりです。

[Try Kotlin](https://try.kotlinlang.org/#/Kotlin%20Koans/Collections/Introduction/Task.kt)

Collections セクションが楽しかったので、JavaScript の Array で同じようなことをやってみました。

JavaScript の構文に慣れるための練習問題になっています。

## このプロジェクトについて

| ディレクトリ | 説明                                                 |
| ------------ | ---------------------------------------------------- |
| documents    | 思ったことや考えたことを垂れ流したドキュメントを格納 |
| koans        | 練習問題を格納                                       |
| samples      | サンプルコードを格納                                 |
