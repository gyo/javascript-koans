# 配列（Array）メソッド概要

[Array - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)

## インスタンスメソッド

破壊的なメソッドと非破壊的なメソッドがあります。

※破壊的、非破壊的について `/documents/destructive.md`

### 変更メソッド

- Array.prototype.pop()
- Array.prototype.push()
- ...etc

変更メソッドはすべて破壊的です。

### アクセサメソッド

- Array.prototype.concat()
- Array.prototype.slice()
- ...etc

アクセサメソッドはすべて非破壊的です。

### 反復メソッド

- Array.prototype.forEach()
- Array.prototype.map()
- Array.prototype.filter()
- ...etc

※反復メソッドについて `/documents/array-iteration-methods.md`

## コンストラクタメソッド

- Array.from()
- Array.isArray()
- Array.of()

`Array` オブジェクトの `static` なメソッドです。

なんらかの変数を引数にとり、それに対して配列に関する処理を行います。
