const { shop } = require("./testData");

test("shop.helloWorld", () => {
  expect(shop.helloWorld()).toBe("Hello World");
});
