# Filter Map

`MyShop.js` の `getCitiesCustomersAreFrom()` メソッドと `getCustomersFrom()` メソッドと `getCustomersWithMoreUndeliveredOrdersThanDelivered()` メソッドを実装してください。

実装後、`test.js` を Jest で実行してください。

## Array.prototype.filter()

[Array.prototype.filter() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)

例：`/samples/array-filter.js`

## Array.prototype.map()

[Array.prototype.map() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)

例：`/samples/array-map.js`
