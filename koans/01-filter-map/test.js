const { shop, aomori, fukushima, tokyo, chiba, kanagawa, aichi, toyama, kochi, tottori, yamaguchi, nagasaki, okinawa, massachusetts, mano, hiori, kogane, sakuya, yuika, kaho, juri, natsuha } = require("./testData");

test("shop.getCitiesCustomersAreFrom", () => {
  const correct = [tokyo, tokyo, massachusetts, nagasaki, kanagawa, kochi, fukushima, aomori, tokyo, chiba, kanagawa, tottori, aichi, toyama, toyama, yamaguchi];
  const result = shop.getCitiesCustomersAreFrom();

  expect(result).toEqual(correct);
});

test("shop.getCustomersFrom : 3 items", () => {
  const correct = [mano, hiori, kaho];
  const result = shop.getCustomersFrom(tokyo);

  expect(result).toEqual(correct);
});

test("shop.getCustomersFrom : 1 item", () => {
  const correct = [kogane];
  const result = shop.getCustomersFrom(nagasaki);

  expect(result).toEqual(correct);
});

test("shop.getCustomersFrom : 0 item", () => {
  const correct = [];
  const result = shop.getCustomersFrom(okinawa);

  expect(result).toEqual(correct);
});

test("shop.getCustomersWithMoreUndeliveredOrdersThanDelivered", () => {
  const correct = [sakuya, yuika, juri, natsuha];
  const result = shop.getCustomersWithMoreUndeliveredOrdersThanDelivered();

  expect(result).toEqual(correct);
});
