# All Any Count Find

`MyShop.js` の `checkAllCustomersAreFrom()` メソッドと `hasCustomerFrom()` メソッドと `findAnyCustomerFrom()` メソッドを実装してください。

`findAnyCustomerFrom()` メソッドでは、該当する顧客が複数いる場合でもいずれかひとりのみ取得できればよいこととします。

実装後、`test.js` を Jest で実行してください。

## Array.prototype.every()

[Array.prototype.every() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every)

例：`/samples/array-every.js`

## Array.prototype.some()

[Array.prototype.some() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some)

例：`/samples/array-some.js`

## Array.prototype.find()

[Array.prototype.find() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find)

例：`/samples/array-find.js`
