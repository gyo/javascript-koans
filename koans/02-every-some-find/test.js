const { shop, shopWithTokyoCustomers, tokyo, okinawa, mano } = require("./testData");

test("shop.checkAllCustomersAreFrom : true", () => {
  const correct = true;
  const result = shopWithTokyoCustomers.checkAllCustomersAreFrom(tokyo);

  expect(result).toEqual(correct);
});

test("shop.checkAllCustomersAreFrom : false", () => {
  const correct = false;
  const result = shop.checkAllCustomersAreFrom(tokyo);

  expect(result).toEqual(correct);
});

test("shop.hasCustomerFrom : true", () => {
  const correct = true;
  const result = shop.hasCustomerFrom(tokyo);

  expect(result).toEqual(correct);
});

test("shop.hasCustomerFrom : false", () => {
  const correct = false;
  const result = shop.hasCustomerFrom(okinawa);

  expect(result).toEqual(correct);
});

test("shop.findAnyCustomerFrom : exist", () => {
  const correct = mano;
  const result = shop.findAnyCustomerFrom(tokyo);

  expect(result).toEqual(correct);
});

test("shop.findAnyCustomerFrom : none", () => {
  const correct = null;
  const result = shop.findAnyCustomerFrom(okinawa);

  expect(result).toEqual(correct);
});
