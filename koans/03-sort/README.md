# Sort

`MyShop.js` の `getCustomersSortedByNumberOfOrders()` メソッドを実装してください。

`getCustomersSortedByNumberOfOrders()` メソッドでは、もとの `this.customers` を変更しないようにしてください。

実装後、`test.js` を Jest で実行してください。

## Array.prototype.sort()

[Array.prototype.sort() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort)

例：`/samples/array-sort.js`
