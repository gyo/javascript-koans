const { shop, mano, hiori, meguru, kogane, mamimi, sakuya, yuika, kiriko, kaho, chiyoko, juri, rinze, natsuha, amana, tenka, chiyuki } = require("./testData");

test("shop.getCustomersSortedByNumberOfOrders", () => {
  const correct = [kaho, chiyuki, tenka, amana, rinze, sakuya, kiriko, juri, mamimi, yuika, chiyoko, natsuha, kogane, hiori, meguru, mano];
  const result = shop.getCustomersSortedByNumberOfOrders();

  expect(result).toEqual(correct);
});

test("shop.getCustomersSortedByNumberOfOrders : side effect", () => {
  const correct = [mano, hiori, meguru, kogane, mamimi, sakuya, yuika, kiriko, kaho, chiyoko, juri, rinze, natsuha, amana, tenka, chiyuki];
  const result = shop.customers;

  expect(result).toEqual(correct);
});
