const { shop, shopWithNoCustomer, brendCoffee, coffeeLatte, americanCoffee, honeyCafeAuLait, soyLatte, coffeeMocha, cappuccino, espressoCoffee, mano, sakuya } = require("./testData");

test("shop.getOrderedProducts : some", () => {
  const correct = [honeyCafeAuLait, soyLatte, coffeeMocha];
  const result = sakuya.getOrderedProducts();

  expect(result).toEqual(correct);
});

test("shop.getOrderedProducts : none", () => {
  const correct = [];
  const result = mano.getOrderedProducts();

  expect(result).toEqual(correct);
});

test("shop.allOrderedProducts : some", () => {
  const correct = [brendCoffee, coffeeLatte, americanCoffee, honeyCafeAuLait, soyLatte, coffeeMocha, cappuccino, espressoCoffee, brendCoffee, coffeeLatte, americanCoffee, honeyCafeAuLait, soyLatte, coffeeMocha, cappuccino, espressoCoffee, brendCoffee, coffeeLatte, americanCoffee, soyLatte, soyLatte, brendCoffee, soyLatte, soyLatte, coffeeLatte, soyLatte, soyLatte, americanCoffee];
  const result = shop.allOrderedProducts();

  expect(result).toEqual(correct);
});

test("shop.allOrderedProducts : none", () => {
  const correct = [];
  const result = shopWithNoCustomer.allOrderedProducts();

  expect(result).toEqual(correct);
});
