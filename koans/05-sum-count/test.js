const { shop, shopWithNoCustomer, tokyo, okinawa, mano, sakuya } = require("./testData");

test("customer.getTotalOrderPrice : some", () => {
  const correct = 980;
  const result = sakuya.getTotalOrderPrice();

  expect(result).toBe(correct);
});

test("customer.getTotalOrderPrice : zero", () => {
  const correct = 0;
  const result = mano.getTotalOrderPrice();

  expect(result).toBe(correct);
});

test("shop.countCustomersFrom : some", () => {
  const correct = 3;
  const result = shop.countCustomersFrom(tokyo);

  expect(result).toBe(correct);
});

test("shop.countCustomersFrom : none", () => {
  const correct = 0;
  const result = shop.countCustomersFrom(okinawa);

  expect(result).toBe(correct);
});

test("shop.countCustomersFrom : no customer", () => {
  const correct = 0;
  const result = shopWithNoCustomer.countCustomersFrom(tokyo);

  expect(result).toBe(correct);
});
