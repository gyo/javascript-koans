# Max Min

`MyShop.js` の `getCustomerWithMaximumNumberOfOrders()` メソッドを実装してください。

`getCustomerWithMaximumNumberOfOrders()` メソッドでは、該当する顧客が複数いる場合でもいずれかひとりのみ取得できればよいこととします。

`MyCustomer.js` の `getMostExpensiveOrderedProduct()` メソッドを実装してください。

`getMostExpensiveOrderedProduct()` メソッドでは、該当する商品が複数ある場合でもいずれかひとつのみ取得できればよいこととします。

実装後、`test.js` を Jest で実行してください。

## Max

JavaScript には `max()` は存在しないので、ビルドインメソッドを組み合わせて実現する必要があります。

例：`/samples/array-max.js`
