const { shop, shopWithNoCustomer, soyLatte, coffeeMocha, mano, sakuya, amana } = require("./testData");

test("shop.getCustomerWithMaximumNumberOfOrders : some", () => {
  const correct = sakuya;
  const result = shop.getCustomerWithMaximumNumberOfOrders();

  expect(result).toEqual(correct);
});

test("shop.getCustomerWithMaximumNumberOfOrders : none", () => {
  const correct = null;
  const result = shopWithNoCustomer.getCustomerWithMaximumNumberOfOrders();

  expect(result).toBe(correct);
});

test("customer.getMostExpensiveOrderedProduct : multiple", () => {
  const correct = soyLatte;
  const result = amana.getMostExpensiveOrderedProduct();

  expect(result).toEqual(correct);
});

test("customer.getMostExpensiveOrderedProduct : single", () => {
  const correct = coffeeMocha;
  const result = sakuya.getMostExpensiveOrderedProduct();

  expect(result).toEqual(correct);
});

test("customer.getMostExpensiveOrderedProduct : single", () => {
  const correct = null;
  const result = mano.getMostExpensiveOrderedProduct();

  expect(result).toBe(correct);
});
