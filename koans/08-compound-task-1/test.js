const { shop, soyLatte, cocoa } = require("./testData");

test("shop.getNumberOfTimesProductWasOrdered : some", () => {
  const correct = 8;
  const result = shop.getNumberOfTimesProductWasOrdered(soyLatte);

  expect(result).toBe(correct);
});

test("shop.getNumberOfTimesProductWasOrdered : none", () => {
  const correct = 0;
  const result = shop.getNumberOfTimesProductWasOrdered(cocoa);

  expect(result).toBe(correct);
});
