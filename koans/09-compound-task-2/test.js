const { shop, shopWithNoCustomer, shopWithSoyLatteCustomers, soyLatte } = require("./testData");

test("shop.getProductsOrderedByAllCustomer : some", () => {
  const correct = [soyLatte, soyLatte];
  const result = shopWithSoyLatteCustomers.getProductsOrderedByAllCustomer();

  expect(result).toEqual(correct);
});

test("shop.getProductsOrderedByAllCustomer : none", () => {
  const correct = [];
  const result = shop.getProductsOrderedByAllCustomer();

  expect(result).toEqual(correct);
});

test("shop.getProductsOrderedByAllCustomer : no customer", () => {
  const correct = [];
  const result = shopWithNoCustomer.getProductsOrderedByAllCustomer();

  expect(result).toEqual(correct);
});
