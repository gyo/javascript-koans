const { soyLatte, mano, sakuya, juri } = require("./testData");

test("customer.getMostExpensiveDeliveredProduct : some", () => {
  const correct = soyLatte;
  const result = sakuya.getMostExpensiveDeliveredProduct();

  expect(result).toEqual(correct);
});

test("customer.getMostExpensiveDeliveredProduct : none", () => {
  const correct = null;
  const result = mano.getMostExpensiveDeliveredProduct();

  expect(result).toEqual(correct);
});

test("customer.getMostExpensiveDeliveredProduct : not delivered", () => {
  const correct = null;
  const result = juri.getMostExpensiveDeliveredProduct();

  expect(result).toEqual(correct);
});
