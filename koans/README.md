# JavaScript Koans

## 進め方

1.  プロジェクトルートで、`npm install` してください。
1.  `/koans/00-introduction/README.md` に課題が書いてあります。読んでください。
1.  `/koans/00-introduction/MyShop.js` をエディタで開き、JSDoc とメソッド名をヒントに `helloWorld()` を実装してください。
1.  `/koans/00-introduction/test.js` を Jest で実行してみてください。例：`npx jest koans/00-introduction/test.js`
1.  すべてのテストが `passed` となっている場合、おめでとうございます、正しく実装できています。`failed` と表示されるものがある場合、何かが間違っています。`/koans/classes/samples/` に実装例があるので参考にしてください。
1.  `/koans/01-` 以降も同様に進めてください。
