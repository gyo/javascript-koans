const Shop = require("./Shop");

class GyoShop extends Shop {
  /**
   * 店舗
   * @param {String} name 店舗名
   * @param {Array<Customer>} customers Customer の一覧
   */
  constructor(name, customers) {
    super(name, customers);
  }

  /**
   * @returns {String} "Hello World" という文字列
   */
  helloWorld() {
    return "Hello World";
  }

  /**
   * @returns {Array<City>} City の配列
   */
  getCitiesCustomersAreFrom() {
    return this.customers.map(customer => {
      return customer.city;
    });
  }

  /**
   * @param {City} city 都市
   * @returns {Array<Customer>} Customer の配列
   */
  getCustomersFrom(city) {
    return this.customers.filter(customer => {
      return customer.city === city;
    });
  }

  /**
   * @returns {Array<Customer>} Customer の配列
   */
  getCustomersWithMoreUndeliveredOrdersThanDelivered() {
    return this.customers.filter(customer => {
      const deliveredOrders = customer.orders.filter(order => {
        return order.isDelivered;
      });
      const unDeliveredOrdersLength = customer.orders.length - deliveredOrders.length;

      return deliveredOrders.length < unDeliveredOrdersLength;
    });
  }

  /**
   * @param {City} city 都市
   * @returns {Boolean} 結果
   */
  checkAllCustomersAreFrom(city) {
    return this.customers.every(customer => {
      return customer.city === city;
    });
  }

  /**
   * @param {City} city 都市
   * @returns {Boolean} 結果
   */
  hasCustomerFrom(city) {
    return this.customers.some(customer => {
      return customer.city === city;
    });
  }

  /**
   * @param {City} city 都市名
   * @returns {Customer|null} 顧客。存在しない場合は null
   */
  findAnyCustomerFrom(city) {
    const result = this.customers.find(customer => {
      return customer.city === city;
    });

    return result === undefined ? null : result;
  }

  /**
   * @returns {Array<Customer>} Customer の配列
   */
  getCustomersSortedByNumberOfOrders() {
    return [...this.customers].sort((prevCustomer, nextCustomer) => {
      return prevCustomer.orders.length < nextCustomer.orders.length;
    });
  }

  /**
   * @returns {Array<Product>} Product の配列
   */
  allOrderedProducts() {
    return this.customers.reduce((accumulator, customer) => {
      return accumulator.concat(customer.getOrderedProducts());
    }, []);
  }

  /**
   * @param {City} city 都市
   * @returns {Number} 顧客数
   */
  countCustomersFrom(city) {
    return this.customers.reduce((accumulator, customer) => {
      if (customer.city === city) {
        return accumulator + 1;
      }
      return accumulator;
    }, 0);

    // With filter()
    // return this.customers.filter(customer => {
    //   return customer.city === city;
    // }).length;
  }

  /**
   * @returns {Customer|null} 顧客。存在しない場合は null
   */
  getCustomerWithMaximumNumberOfOrders() {
    return this.customers.reduce((accumulator, customer) => {
      if (accumulator === null) {
        return customer;
      }

      if (accumulator.orders.length < customer.orders.length) {
        return customer;
      }

      return accumulator;
    }, null);
  }

  /**
   * @returns {Object<String, Array<Customer>>} key : cityName, value : Customer の配列
   */
  groupCustomersByCityName() {
    return this.customers.reduce((accumulator, customer) => {
      if (accumulator[customer.city.name] === undefined) {
        accumulator[customer.city.name] = [];
      }
      accumulator[customer.city.name].push(customer);
      return accumulator;
    }, {});
  }

  /**
   * @param {Product} product 商品
   * @returns {Number} 注文数
   */
  getNumberOfTimesProductWasOrdered(product) {
    return this.allOrderedProducts().filter(orderedProduct => {
      return orderedProduct === product;
    }).length;
  }

  /**
   * @returns {Array<Product>} Product の配列
   */
  getProductsOrderedByAllCustomer() {
    return this.customers.reduce((accumulator, customer) => {
      return customer.getOrderedProducts().filter(product => {
        return accumulator.includes(product);
      });
    }, this.allOrderedProducts());
  }
}

module.exports = GyoShop;
