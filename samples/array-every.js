const moreThanZero = [1, 2, 3, 4, 5, 6].every(item => {
  return 0 < item;
});

console.log(moreThanZero); // true

const moreThanFive = [1, 2, 3, 4, 5, 6].every(item => {
  return 5 < item;
});

console.log(moreThanFive); // false
