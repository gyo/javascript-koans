const equalToFive = [1, 2, 3, 4, 5, 6].find(item => {
  return item === 5;
});

console.log(equalToFive); // 5

const equalToTen = [1, 2, 3, 4, 5, 6].find(item => {
  return item === 10;
});

console.log(equalToTen); // undefined
