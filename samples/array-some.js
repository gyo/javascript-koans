const moreThanFive = [1, 2, 3, 4, 5, 6].some(item => {
  return 5 < item;
});

console.log(moreThanFive); // true

const moreThanTen = [1, 2, 3, 4, 5, 6].some(item => {
  return 10 < item;
});

console.log(moreThanTen); // false
