const total = [1, 2, 3, 4, 5, 6].reduce((accumulator, item) => {
  return accumulator + item;
}, 0);

console.log(total); // 21
